import { book } from './models/book';

export interface AppState {
    readonly lbrBook: book[];
}