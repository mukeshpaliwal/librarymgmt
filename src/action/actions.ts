import { Action } from '@ngrx/store';
import { book } from '../models/book';

export const ADD_BOOK = "ADD BOOK";
export const REMOVE_BOOK = "REMOVE BOOK";
export const SEARCH_BOOK = "SEARCH BOOK";
export const DO_FILTER = "FILTER BOOK";

export class AddBook implements Action {
    readonly type = ADD_BOOK;
    constructor(public libBook: book) {}
}

export class RemoveBook implements Action {
    readonly type = REMOVE_BOOK;
    constructor(public libBook: number) {}
}

export class SearchBook implements Action {
    readonly type = SEARCH_BOOK;
    constructor(public libBook: String) {}
}

export class DoFilter implements Action {
    readonly type = DO_FILTER;
    constructor(public libBook: String) {}
}

export type Actions = AddBook | RemoveBook | SearchBook | DoFilter;