export const BOOKS = [
  {
    "title": "Unlocking Android",
    "srn": 1933988673,
    "count": 6,
    "authors": ["W. Frank Ableson", "Charlie Collins", "Robi Sen"],
    "issued":0
  },
  {
    "title": "Android in Action, Second Edition",
    "srn": 1935182722,
    "count": 3,
    "authors": ["W. Frank Ableson", "Robi Sen"],
    "issued":1
  },
  {
    "title": "Specification by Example",
    "srn": 1617290084,
    "count": 10,
    "authors": ["Gojko Adzic"],
    "issued":7
  },
  {
    "title": "Flex 3 in Action",
    "srn": 1933988746,
    "count": 7,
    "authors": ["Tariq Ahmed with Jon Hirschi", "Faisal Abid"],
    "issued":5
  },
  {
    "title": "Flex 4 in Action",
    "srn": 1935182420,
    "count": 6,
    "authors": ["Tariq Ahmed", "Dan Orlando", "John C. Bland II", "Joel Hooks"],
    "issued":2
  },
  {
    "title": "Collective Intelligence in Action",
    "srn": 1933988312,
    "count": 5,
    "authors": ["Satnam Alag"],
    "issued":5
  },
  {
    "title": "Zend Framework in Action",
    "srn": 1933988320,
    "count": 4,
    "authors": ["Rob Allen", "Nick Lo", "Steven Brown"],
    "issued":1
  },
  {
    "title": "Flex on Java",
    "srn": 1933988797,
    "count": 3,
    "authors": ["Bernerd Allmon", "Jeremy Anderson"],
    "issued":1
  },
  {
    "title": "Griffon in Action",
    "srn": 1935182234,
    "count": 2,
    "authors": ["Andres Almiray", "Danno Ferrin","James Shingler"],
    "issued":1
  },
  {
    "title": "OSGi in Depth",
    "srn": 1935182176,
    "count": 6,
    "authors": ["Alexandre de Castro Alves"],
    "issued":1
  },
  {
    "title": "Flexible Rails",
    "srn": 1933988509,
    "count": 2,
    "authors": ["Peter Armstrong"],
    "issued":1
  },
  {
    "title": "Hello! Flex 4",
    "srn": 1933988762,
    "count": 4,
    "authors": ["Peter Armstrong"],
    "issued":1
  },
  {
    "title": "Coffeehouse",
    "srn": 1884777384,
    "count": 1,
    "authors": ["Levi Asher", "Christian Crumlish"],
    "issued":1
  },
  {
    "title": "Team Foundation Server 2008 in Action",
    "srn": 1933988592,
    "count": 3,
    "authors": ["Jamil Azher"],
    "issued":3
  },
  {
    "title": "Brownfield Application Development in .NET",
    "srn": 1933988711,
    "count": 5,
    "authors": ["Kyle Baley", "Donald Belcham"],
    "issued":1
  },
  {
    "title": "MongoDB in Action",
    "srn": 1935182870,
    "count": 3,
    "authors": ["Kyle Banker"],
    "issued":1
  }
]