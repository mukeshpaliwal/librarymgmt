import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { book } from '../../../models/book';
import { AppState } from '../../../state';
import * as Action from '../../../action/actions';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.less']
})
export class HomeComponent implements OnInit {

  bookList: Observable<book[]>;
  filterList:Array<String> = ["All", "Available", "Unavailable", "Issued"];
  filter:String = "All";
  constructor(public store: Store<AppState>) {
    this.bookList = store.select('lbrBook');
  }

  ngOnInit() {
    document.getElementById("loginIcon").style.display = "block";
  }

  onDelete = index => {
    this.store.dispatch(new Action.RemoveBook(index));
  }

  onChange = evt => {
    let currentVal = evt.currentTarget.value;
    this.store.dispatch(new Action.DoFilter(currentVal));
  }

  onSearch = evt => {
    let searchText = evt.currentTarget.value.toLowerCase();
    this.store.dispatch(new Action.SearchBook(searchText));
  }

  onSubmit = (form: NgForm) => {
    if(isNaN(form.value.srn) || isNaN(form.value.count)) {
      alert("Please enter valid Serial number and count");
      return;
    } else if(form.value.title == "" || form.value.authors == "") {
      alert("Please enter the book title and author name");
      return;
    }

    form.value.authors = [form.value.authors];
    form.value["issued"] = 0;
    this.store.dispatch(new Action.AddBook(form.value));
    form.resetForm();
    document.getElementById("modalCloseBtn").click();
  }
}
