import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.less']
})
export class LoginComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
    document.getElementById("loginIcon").style.display = "none";
  }

  onSubmit(form: NgForm) {
    if(form.value.login === "admin" && form.value.password === "admin") {
      this.router.navigate(["home"]);
    } else {
      alert("Please enter Username: admin & Password: admin");
    }
  }

}
