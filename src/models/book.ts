export interface book {
    srn: number;
    title: string;
    count: number;
    authors: Array<string>;
    issued: number;
}