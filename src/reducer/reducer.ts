import { book } from '../models/book';
import * as Actions from '../action/actions';
import { BOOKS } from '../app/constants/books';

const initialState: Array<book> = BOOKS;
const originalState: Array<book> = BOOKS;
let searchText:String = "";
let selectedType:String = "All";

export function reducer(state: book[] = initialState, action: Actions.Actions, originalData: book[] = originalState ) {
    switch(action.type) {
        case Actions.ADD_BOOK :
            state = [...state, action.libBook];
            originalData.push(action.libBook);
            state = getFilter(selectedType);
            state = state.filter(filterOnSearch);
            return state;
        case Actions.REMOVE_BOOK :
            state.splice(action.libBook, 1);
            originalData.splice(action.libBook, 1);
            return state;
        case Actions.SEARCH_BOOK :
            searchText = action.libBook;
            state = getFilter(selectedType);
            state = state.filter(filterOnSearch);
            return state;
        case Actions.DO_FILTER :
            selectedType = action.libBook;
            state = getFilter(action.libBook);
            state = state.filter(filterOnSearch);
            return state;
        default:
            return state;
    }

    function getFilter(val) {
        switch(val) {
            case "All" :
                return  originalData;
            case "Available":
                return originalData.filter(filterAvailableData);
            case "Unavailable":
                return  originalData.filter(filterUnavailableData);
            case "Issued":
                return  originalData.filter(filterIssuedData);
            default :
                return originalData;
        }
    }

    function filterAvailableData(data) { 
        if(data.count > 0 && data.issued < data.count) {
            return true;
        } 
    } 

    function filterUnavailableData(data) { 
        if(data.count == 0 || data.count == data.issued) {
            return true;
        } 
    } 

    function filterIssuedData(data) { 
        if(data.issued > 0) {
            return true;
        } 
    }

    function filterOnSearch(data) {
        for(let key in data) {
            let dataVal = data[key];
            if( typeof(dataVal) === "number") {
                dataVal = dataVal.toString();
            }
            if( typeof(dataVal) === "object") {
                dataVal = dataVal.join(",");
            }
            if(dataVal.toLowerCase().indexOf(searchText) > -1) {
                return true;
            }
        }
    }
}